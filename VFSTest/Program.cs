﻿using System;
using JfVfs;
using JfVfs.Logging;

namespace VFSTest
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            string source = "D:\\";
            string target = "R:\\";
            if (args.Length > 0)
                source = args[0];
            if (args.Length > 1)
                target = args[1];
            Console.CancelKeyPress += (sender, eventArgs) =>
            {
                Console.WriteLine("Received cancel key, unmounting file system");
                FsHost.Unmount(target);
            };
            Console.WriteLine(FsHost.GetVersionString());
            FsHost.Unmount(target);
            FsHost.Mount(target, new OverlayFs(source).Wrap(), debug: false, logger: new LevelLogger(LogLevel.Warn));
        }
    }
}