﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using JfVfs;
using SharpFileSystem;
using SharpFileSystem.FileSystems;

namespace VFSTest
{
    public class OverlayFs : IFileSystemExt
    {
        private readonly List<FileSystemPath> _deleted;
        private readonly DriveInfo _info;
        private readonly MemoryFileSystem _mfs;
        private readonly PhysicalFileSystem _pfs;

        public OverlayFs(string fsRoot)
        {
            _pfs = new PhysicalFileSystem(fsRoot);
            _mfs = new MemoryFileSystem();
            _deleted = new List<FileSystemPath>();
            _info = DriveInfo.GetDrives().FirstOrDefault(s => fsRoot.StartsWith(s.Name)) ?? DriveInfo.GetDrives()[0];
        }

        public void Dispose()
        {
            _pfs.Dispose();
            _mfs.Dispose();
        }

        public ICollection<FileSystemPath> GetEntities(FileSystemPath path)
        {
            List<FileSystemPath> entities = new List<FileSystemPath>();
            try
            {
                if (IsDeleted(path))
                    throw new DirectoryNotFoundException();
                foreach (IFileSystem fs in new IFileSystem[] {_pfs, _mfs})
                    if (fs.Exists(path))
                        foreach (FileSystemPath entity in fs.GetEntities(path))
                            if (!IsDeleted(entity) && !entities.Contains(entity))
                                entities.Add(entity);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return entities;
        }

        public bool Exists(FileSystemPath path) =>
            path.IsRoot || !IsDeleted(path) && (_pfs.Exists(path) || _mfs.Exists(path));

        public Stream CreateFile(FileSystemPath path)
        {
            Undelete(path);
            if (Exists(path.ParentPath))
                return _mfs.CreateFile(path);
            throw new DirectoryNotFoundException();
        }

        public Stream OpenFile(FileSystemPath path, FileAccess access)
        {
            if (!IsDeleted(path))
            {
                if (!_mfs.Exists(path))
                {
                    if (access == FileAccess.Read)
                        return _pfs.OpenFile(path, FileAccess.Read);
                    if (!path.IsRoot && !_mfs.Exists(path.ParentPath))
                        _mfs.CreateDirectoryRecursive(path.ParentPath);
                    using Stream rs = _pfs.OpenFile(path, FileAccess.Read);
                    using Stream ws = _mfs.CreateFile(path);
                    rs.CopyTo(ws);
                }
                return _mfs.OpenFile(path, access);
            }
            throw new FileNotFoundException();
        }

        public void CreateDirectory(FileSystemPath path)
        {
            Undelete(path);
            if (Exists(path.ParentPath))
                _mfs.CreateDirectory(path);
            else
                throw new DirectoryNotFoundException();
        }

        public void Delete(FileSystemPath path)
        {
            if (!Exists(path))
            {
                if (path.IsDirectory)
                    throw new DirectoryNotFoundException();
                throw new FileNotFoundException();
            }
            if (_mfs.Exists(path))
                _mfs.Delete(path);
            if (_pfs.Exists(path) && !_deleted.Contains(path))
                _deleted.Add(path);
        }

        public void GetSpace(out long totalSize, out long usedSpace)
        {
            DriveInfo info = DriveInfo.GetDrives().FirstOrDefault(s => s.Name == _info.Name) ??
                             DriveInfo.GetDrives()[0];
            totalSize = info.TotalSize;
            usedSpace = totalSize - info.AvailableFreeSpace;
        }

        public bool ReadOnly { get; } = true;

        private bool IsDeleted(FileSystemPath path)
        {
            while (true)
            {
                if (path.IsRoot)
                    return false;
                if (_deleted.Contains(path))
                    return true;
                path = path.ParentPath;
            }
        }

        private void Undelete(FileSystemPath path)
        {
            while (true)
            {
                if (path.IsRoot)
                    return;
                if (_deleted.Contains(path))
                {
                    _deleted.Remove(path);
                    if (path.IsDirectory && _pfs.Exists(path))
                        foreach (FileSystemPath entity in _pfs.GetEntities(path))
                            Delete(entity);
                }
                path = path.ParentPath;
            }
        }
    }
}