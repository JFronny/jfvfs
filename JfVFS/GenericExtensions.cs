﻿using System.IO;
using System.Text;
using JfVfs.Internal;
using SharpFileSystem;
using SharpFileSystem.IO;

namespace JfVfs
{
    public static class GenericExtensions
    {
        public static void WriteAllText(this IFileSystem fs, FileSystemPath path, string text)
        {
            byte[] data = Encoding.UTF8.GetBytes(text);
            if (fs.Exists(path))
            {
                using Stream v = fs.OpenFile(path, FileAccess.Write);
                v.Position = 0;
                v.SetLength(data.Length);
                v.Write(data);
            }
            else
            {
                using Stream v = fs.CreateFile(path);
                v.Write(data);
            }
        }

        public static string ReadAllText(this IFileSystem fs, FileSystemPath path)
        {
            using Stream v = fs.OpenFile(path, FileAccess.Read);
            return Encoding.UTF8.GetString(v.ReadAllBytes());
        }

        public static IFileSystemExt Wrap<T>(this T fs) where T : IFileSystem
        {
            if (typeof(IFileSystemExt).IsAssignableFrom(typeof(T)))
                return (IFileSystemExt) fs;
            return new FileSystemW(fs);
        }
    }
}