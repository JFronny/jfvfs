﻿using System;

namespace JfVfs
{
    public class VersionInfo
    {
        public Version DokanDriver { get; internal set; }
        public Version DokanLib { get; internal set; }
        public Version DokanNet { get; internal set; }
        public Version SharpCoreFs { get; internal set; }
        public Version JfVfs { get; internal set; }

        public override string ToString() =>
            $@"Dokan Driver: {DokanDriver}
Dokan Native: {DokanLib}
Dokan.Net: {DokanNet}
SharpCoreFS: {SharpCoreFs}
JF.VFS: {JfVfs}";
    }
}