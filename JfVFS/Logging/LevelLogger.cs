﻿using System;
using DokanNet.Logging;

namespace JfVfs.Logging
{
    public class LevelLogger : LevelLogger<ConsoleLogger>, IDisposable
    {
        private readonly ConsoleLogger _l;

        public LevelLogger(LogLevel level = LogLevel.Info, string loggerName = "") : base(level)
        {
            _l = new ConsoleLogger(loggerName);
            Logger = _l;
        }

        public void Dispose() => _l?.Dispose();
    }

    public class LevelLogger<T> : ILogger where T : ILogger
    {
        public T Logger;
        public LogLevel LogLevel;

        public LevelLogger(LogLevel level = LogLevel.Info) => LogLevel = level;

        public void Debug(string message, params object[] args)
        {
            if (LogLevel.Debug >= LogLevel)
                Logger.Debug(message, args);
        }

        public void Info(string message, params object[] args)
        {
            if (LogLevel.Info >= LogLevel)
                Logger.Info(message, args);
        }

        public void Warn(string message, params object[] args)
        {
            if (LogLevel.Warn >= LogLevel)
                Logger.Warn(message, args);
        }

        public void Error(string message, params object[] args)
        {
            if (LogLevel.Error >= LogLevel)
                Logger.Error(message, args);
        }

        public void Fatal(string message, params object[] args)
        {
            if (LogLevel.Fatal >= LogLevel)
                Logger.Fatal(message, args);
        }
    }
}