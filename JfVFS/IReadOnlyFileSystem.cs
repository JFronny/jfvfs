﻿using System.Collections.Generic;
using System.IO;
using JfVfs.Internal;
using SharpFileSystem;

namespace JfVfs
{
    public abstract class ReadOnlyFileSystem : IFileSystemExt
    {
        public void Dispose()
        {
        }

        public abstract ICollection<FileSystemPath> GetEntities(FileSystemPath path);
        public abstract bool Exists(FileSystemPath path);
        public Stream CreateFile(FileSystemPath path) => throw new ReadOnlyException();

        public Stream OpenFile(FileSystemPath path, FileAccess access)
        {
            if (access != FileAccess.Read)
                throw new ReadOnlyException();
            return OpenReadOnly(path);
        }

        public void CreateDirectory(FileSystemPath path) => throw new ReadOnlyException();
        public void Delete(FileSystemPath path) => throw new ReadOnlyException();
        public bool ReadOnly { get; } = true;

        public void Move(IFileSystem destinationSystem, FileSystemPath source, FileSystemPath destination) =>
            throw new ReadOnlyException();

        public void Copy(IFileSystem destinationSystem, FileSystemPath source, FileSystemPath destination) =>
            throw new ReadOnlyException();

        public abstract Stream OpenReadOnly(FileSystemPath path);
    }
}