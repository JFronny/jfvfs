﻿using System;
using System.Reflection;
using DokanNet;
using DokanNet.Logging;
using JfVfs.Internal;
using JfVfs.Logging;
using SharpFileSystem;

namespace JfVfs
{
    public static class FsHost
    {
        private const string VolumeLabel = "JF FsWrap";
        private const string FileSystemName = "FsWrap VFS (c) JFronny 2020";

        public static void Mount(char driveLetter, IFileSystemExt fileSystem, string volumeLabel = VolumeLabel,
            string fileSystemName = FileSystemName, bool debug = false, ILogger logger = null) => Mount(
            $"{driveLetter}:\\", fileSystem,
            volumeLabel, fileSystemName, debug, logger);

        public static void Mount(string mountPoint, IFileSystemExt fileSystem, string volumeLabel = VolumeLabel,
            string fileSystemName = FileSystemName, bool debug = false, ILogger logger = null)
        {
            //Properly format root drives
            if (IsRoot(mountPoint, out char? drive))
                Mount(drive.Value, fileSystem, volumeLabel, fileSystemName, debug, logger);
            //Add entity movers and copiers for the file system if the user has not done so yet
            Type t = fileSystem.GetType();
            if (!EntityMovers.Registration.TryGetSupported(t, t, out _))
                EntityMovers.Registration.AddLast(t, t,
                    new FsExtEntityMover((sf, sp, df, dp)
                        => sf.Wrap().Move(df, sp, dp)));
            if (!EntityCopiers.Registration.TryGetSupported(t, t, out _))
                EntityCopiers.Registration.AddLast(t, t,
                    new FsExtEntityCopier((sf, sp, df, dp)
                        => sf.Wrap().Copy(df, sp, dp)));
            //Fix logger if null
            logger ??= new LevelLogger(debug ? LogLevel.Debug : LogLevel.Info);
            try
            {
                //Create a simple folder structure. Useful for testing simple file systems
                if (debug)
                {
                    fileSystem.CreateDirectory(FileSystemPath.Parse("/info/"));
                    fileSystem.WriteAllText(FileSystemPath.Parse("/info.txt"), "Hello, world!");
                    fileSystem.WriteAllText(FileSystemPath.Parse("/info/info.txt"), "Hello, recursive world!");
                    logger.Debug(fileSystem.ReadAllText(FileSystemPath.Parse("/info/info.txt")));
                }
                IDokanOperations mfs = new FsWrap(fileSystem, volumeLabel, fileSystemName, logger);
                mfs.Mount(mountPoint,
                    (debug ? DokanOptions.DebugMode | DokanOptions.StderrOutput : DokanOptions.FixedDrive)
                    | DokanOptions.RemovableDrive, logger
                );
                logger.Info("Mount closed, returning to program");
            }
            catch (DokanException e)
            {
                logger.Fatal("Error: " + e.Message);
            }
        }

        public static void Unmount(char driveLetter) => Dokan.Unmount(driveLetter);

        public static void Unmount(string mountPoint)
        {
            if (IsRoot(mountPoint, out char? drive))
                Unmount(drive.Value);
            else
                Dokan.RemoveMountPoint(mountPoint);
        }

        private static bool IsRoot(string mountPoint, out char? drive)
        {
            if (mountPoint.Length == 1 || mountPoint.Length == 2 && mountPoint[1] == ':')
            {
                drive = mountPoint[0];
                return true;
            }
            drive = null;
            return false;
        }

        public static VersionInfo GetVersion() =>
            new VersionInfo
            {
                DokanDriver = ParseDokan(Dokan.DriverVersion),
                DokanLib = ParseDokan(Dokan.Version),
                DokanNet = typeof(Dokan).Assembly.GetName().Version,
                SharpCoreFs = typeof(File).Assembly.GetName().Version,
                JfVfs = typeof(FsHost).Assembly.GetName().Version
            };

        public static string GetVersionString()
        {
            VersionInfo info = GetVersion();
            return
                $"{info}\n{Assembly.GetCallingAssembly().GetName().Name}: {Assembly.GetCallingAssembly().GetName().Version}";
        }

        private static Version ParseDokan(int ver)
        {
            try
            {
                return Version.Parse(string.Join('.', ver.ToString().ToCharArray()));
            }
            catch
            {
                return new Version(ver, 0);
            }
        }
    }
}