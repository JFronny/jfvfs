﻿using System;

namespace JfVfs.Internal
{
    public class ReadOnlyException : InvalidOperationException
    {
        public ReadOnlyException() : base("This is a read-only filesystem.")
        {
        }
    }
}