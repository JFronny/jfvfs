﻿using System;
using SharpFileSystem;

namespace JfVfs.Internal
{
    internal class FsExtEntityCopier : IEntityCopier
    {
        private readonly Action<IFileSystem, FileSystemPath, IFileSystem, FileSystemPath> _copy;
        public FsExtEntityCopier(Action<IFileSystem, FileSystemPath, IFileSystem, FileSystemPath> copy) => _copy = copy;

        public void Copy(IFileSystem source, FileSystemPath sourcePath, IFileSystem destination,
            FileSystemPath destinationPath) => _copy(source, sourcePath, destination, destinationPath);
    }
}