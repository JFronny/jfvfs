﻿using System.Collections.Generic;
using System.IO;
using SharpFileSystem;

namespace JfVfs.Internal
{
    internal class FileSystemW : IFileSystemExt
    {
        public static StandardEntityCopier _copier = new StandardEntityCopier();
        public static StandardEntityMover _mover = new StandardEntityMover();
        private readonly IFileSystem _fs;
        public FileSystemW(IFileSystem fs) => _fs = fs;
        public void Dispose() => _fs.Dispose();
        public ICollection<FileSystemPath> GetEntities(FileSystemPath path) => _fs.GetEntities(path);
        public bool Exists(FileSystemPath path) => _fs.Exists(path);
        public Stream CreateFile(FileSystemPath path) => _fs.CreateFile(path);
        public Stream OpenFile(FileSystemPath path, FileAccess access) => _fs.OpenFile(path, access);
        public void CreateDirectory(FileSystemPath path) => _fs.CreateDirectory(path);
        public void Delete(FileSystemPath path) => _fs.Delete(path);
        public bool ReadOnly { get; } = false;

        public void Move(FileSystemPath source, FileSystemPath destination) =>
            _mover.Move(_fs, source, _fs, destination);

        public void Copy(FileSystemPath source, FileSystemPath destination) =>
            _copier.Copy(_fs, source, _fs, destination);
    }
}