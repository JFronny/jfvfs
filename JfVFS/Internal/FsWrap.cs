﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DokanNet;
using DokanNet.Logging;
using SharpFileSystem;
using FileAccess = DokanNet.FileAccess;

namespace JfVfs.Internal
{
    //TODO fix: two saving in notepad clears the file if another instance of notepad is open
    internal class FsWrap : IDokanOperations
    {
        private readonly string _fileSystemName;
        private readonly IFileSystemExt _fs;
        private readonly ILogger _logger;
        private readonly string _volumeLabel;

        public FsWrap(IFileSystemExt fs, string volumeLabel, string fileSystemName, ILogger logger)
        {
            _fs = fs;
            _volumeLabel = volumeLabel;
            _fileSystemName = fileSystemName;
            _logger = logger;
        }

        public NtStatus CreateFile(string fileName, FileAccess access, FileShare share, FileMode mode,
            FileOptions options,
            FileAttributes attributes, IDokanFileInfo info)
        {
            try
            {
                if (string.IsNullOrEmpty(fileName))
                    fileName = "\\";
                else if (fileName == "\\*" && access == FileAccess.ReadAttributes)
                    return DokanResult.Success;
                if (fileName == "\\")
                {
                    info.IsDirectory = true;
                    return DokanResult.Success;
                }
                FileSystemPath filePath = FileSystemPath.Parse(FixName(fileName, false));
                FileSystemPath dirPath = FileSystemPath.Parse(FixName(fileName, true));
                switch (mode)
                {
                    case FileMode.CreateNew:
                        if (_fs.ReadOnly)
                            throw new ReadOnlyException();
                        if (_fs.Exists(filePath))
                            return info.IsDirectory ? DokanResult.AlreadyExists : DokanResult.FileExists;
                        if (info.IsDirectory)
                            _fs.CreateDirectory(dirPath);
                        else
                        {
                            _fs.CreateFile(filePath);
                            info.Context = new StreamContext(filePath, FileAccess.WriteData);
                        }
                        return DokanResult.Success;
                    case FileMode.Create:
                        if (_fs.ReadOnly)
                            throw new ReadOnlyException();
                        _fs.CreateFile(filePath);
                        info.Context = new StreamContext(info.IsDirectory ? dirPath : filePath, FileAccess.WriteData);
                        return DokanResult.Success;
                    case FileMode.Open:
                        if (_fs.Exists(filePath))
                        {
                            info.IsDirectory = false;
                            if (access.HasFlag(FileAccess.ReadData) || access.HasFlag(FileAccess.GenericRead))
                                info.Context = new StreamContext(filePath, FileAccess.ReadData);
                            else if (access.HasFlag(FileAccess.WriteData))
                            {
                                info.Context = new StreamContext(filePath, FileAccess.WriteData);
                                if (_fs.ReadOnly)
                                    throw new ReadOnlyException();
                            }
                            else if (access.HasFlag(FileAccess.Delete))
                            {
                                info.Context = new StreamContext(filePath, FileAccess.Delete);
                                if (_fs.ReadOnly)
                                    throw new ReadOnlyException();
                            }
                            else if (!access.HasFlag(FileAccess.ReadAttributes) &&
                                     !access.HasFlag(FileAccess.ReadPermissions))
                                return DokanResult.NotImplemented;
                            return DokanResult.Success;
                        }
                        if (_fs.Exists(dirPath))
                        {
                            info.IsDirectory = true;
                            return DokanResult.Success;
                        }
                        return DokanResult.FileNotFound;
                    case FileMode.OpenOrCreate:
                        if (!_fs.Exists(filePath))
                        {
                            if (_fs.ReadOnly)
                                throw new ReadOnlyException();
                            _fs.CreateFile(filePath);
                        }
                        if (access.HasFlag(FileAccess.ReadData) && !access.HasFlag(FileAccess.WriteData))
                            info.Context = new StreamContext(filePath, FileAccess.ReadData);
                        else
                        {
                            if (_fs.ReadOnly)
                                throw new ReadOnlyException();
                            info.Context = new StreamContext(filePath, FileAccess.WriteData);
                        }
                        return DokanResult.Success;
                    case FileMode.Truncate:
                        return DokanResult.NotImplemented;
                    case FileMode.Append:
                        return DokanResult.NotImplemented;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(mode), mode, null);
                }
            }
            catch (ReadOnlyException)
            {
                return DokanResult.AccessDenied;
            }
        }

        public void Cleanup(string fileName, IDokanFileInfo info)
        {
            try
            {
                if (info.DeleteOnClose)
                    DeleteFile(fileName, info);
                else if (!info.IsDirectory)
                {
                    StreamContext context = info.Context as StreamContext;
                    if (context?.CanWriteDelayed ?? false)
                    {
                        context.Stream.Seek(0, SeekOrigin.Begin);
                        context.Task = Task.Run(() =>
                        {
                            try
                            {
                                using Stream s = _fs.OpenFile(context.File, System.IO.FileAccess.Write);
                                s.Seek(0, SeekOrigin.Begin);
                                s.SetLength(0);
                                context.Stream.CopyTo(s);
                            }
                            catch (Exception e)
                            {
                                _logger.Error("Failed to complete file cleanup task\n{0}", e);
                            }
                        });
                    }
                    if (context?.Task != null)
                    {
                        context.Task.Wait();
                        if (!context.Task.IsCompleted)
                            _logger.Error("Failed to complete file cleanup task");
                        context.Dispose();
                        info.Context = null;
                    }
                }
            }
            catch (ReadOnlyException)
            {
                _logger.Info("Tried to flush read only stream");
            }
        }

        public void CloseFile(string fileName, IDokanFileInfo info)
        {
            (info.Context as StreamContext)?.Dispose();
        }

        public NtStatus ReadFile(string fileName, byte[] buffer, out int bytesRead, long offset, IDokanFileInfo info)
        {
            if (buffer == null)
                throw new ArgumentNullException(nameof(buffer));
            if (offset < 0)
                throw new ArgumentOutOfRangeException(nameof(offset), nameof(offset) + " is below 0");
            StreamContext context = (StreamContext) info.Context;
            lock (context)
            {
                if (context.Stream == null)
                    context.Stream = Stream.Synchronized(_fs.OpenFile(context.File, System.IO.FileAccess.ReadWrite));
                context.Stream.Position = offset;
                bytesRead = context.Stream.Read(buffer, 0, buffer.Length);
            }
            return DokanResult.Success;
        }

        public NtStatus WriteFile(string fileName, byte[] buffer, out int bytesWritten, long offset,
            IDokanFileInfo info)
        {
            try
            {
                if (_fs.ReadOnly)
                    throw new ReadOnlyException();
                if (buffer == null)
                    throw new ArgumentNullException(nameof(buffer));
                if (offset < 0)
                    throw new ArgumentOutOfRangeException(nameof(offset), nameof(offset) + " is below 0");
                if (info == null)
                    throw new ArgumentNullException(nameof(info));

                StreamContext context = (StreamContext) info.Context;

                lock (context)
                {
                    context.Stream ??= Stream.Synchronized(new MemoryStream());

                    context.Stream.Position = offset;
                    context.Stream.Write(buffer, 0, buffer.Length);
                    bytesWritten = (int) (context.Stream.Position - offset);
                }
                return DokanResult.Success;
            }
            catch (ReadOnlyException)
            {
                bytesWritten = 0;
                return DokanResult.AccessDenied;
            }
        }

        public NtStatus FlushFileBuffers(string fileName, IDokanFileInfo info)
        {
            try
            {
                ((StreamContext) info.Context).Stream?.Flush();
                return DokanResult.Success;
            }
            catch (ReadOnlyException)
            {
                return DokanResult.AccessDenied;
            }
            catch (Exception e)
            {
                _logger.Error("Failed to flush file buffer, assuming full disk\n{0}", e);
                return DokanResult.DiskFull;
            }
        }

        public NtStatus GetFileInformation(string fileName, out FileInformation fileInfo, IDokanFileInfo info)
        {
            FileSystemPath fs;
            fileInfo = new FileInformation();
            try
            {
                fileName = FixName(fileName, info.IsDirectory);
                fs = FileSystemPath.Parse(fileName);
                if (!_fs.Exists(fs))
                    return info.IsDirectory ? DokanResult.FileNotFound : DokanResult.PathNotFound;
            }
            catch (Exception e)
            {
                _logger.Error("Failed to parse file path\n{0}", e);
                return DokanResult.Error;
            }
            if (fs.IsDirectory)
                fileInfo.Attributes = FileAttributes.Directory;
            else
            {
                fileInfo.Attributes = FileAttributes.NotContentIndexed;
                if ((info.Context as StreamContext)?.Stream != null)
                    fileInfo.Length = ((StreamContext) info.Context)?.Stream.Length ?? 0;
                else
                {
                    using Stream s = _fs.OpenFile(fs, System.IO.FileAccess.Read);
                    fileInfo.Length = s.Length;
                }
            }
            if (_fs.ReadOnly)
                fileInfo.Attributes |= FileAttributes.ReadOnly;
            fileInfo.FileName = fs.EntityName;
            return DokanResult.Success;
        }

        public NtStatus FindFiles(string fileName, out IList<FileInformation> files, IDokanFileInfo info)
        {
            FileSystemPath fs;
            files = new List<FileInformation>();
            try
            {
                fileName = FixName(fileName, info.IsDirectory);
                fs = FileSystemPath.Parse(fileName);
                if (!_fs.Exists(fs) || !info.IsDirectory)
                    return info.IsDirectory ? DokanResult.FileNotFound : DokanResult.PathNotFound;
            }
            catch (Exception e)
            {
                _logger.Error("Failed to parse file path\n{0}", e);
                return DokanResult.Error;
            }
            files = GetFileInfo(_fs.GetEntities(fs));
            return DokanResult.Success;
        }

        public NtStatus FindFilesWithPattern(string fileName, string searchPattern, out IList<FileInformation> files,
            IDokanFileInfo info)
        {
            FileSystemPath fs;
            files = new List<FileInformation>();
            try
            {
                fileName = FixName(fileName, info.IsDirectory);
                fs = FileSystemPath.Parse(fileName);
                if (!_fs.Exists(fs) || !info.IsDirectory)
                    return info.IsDirectory ? DokanResult.FileNotFound : DokanResult.PathNotFound;
            }
            catch (Exception e)
            {
                _logger.Error("Failed to parse file path\n{0}", e);
                return DokanResult.Error;
            }
            files = GetFileInfo(_fs.GetEntities(fs).Where(i =>
            {
                _logger.Debug(searchPattern.Contains('?') || searchPattern.Contains('*')
                    ? searchPattern.Replace('?', '.').Replace("*", ".*")
                    : "^" + searchPattern + "$");
                _logger.Debug(i.EntityName);
                return Regex.IsMatch(i.EntityName,
                    searchPattern.Contains('?') || searchPattern.Contains('*')
                        ? searchPattern.Replace('?', '.').Replace("*", ".*")
                        : "^" + searchPattern + "$");
            }));
            return DokanResult.Success;
        }

        public NtStatus SetFileAttributes(string fileName, FileAttributes attributes, IDokanFileInfo info) =>
            DokanResult.Success;

        public NtStatus SetFileTime(string fileName, DateTime? creationTime, DateTime? lastAccessTime,
            DateTime? lastWriteTime,
            IDokanFileInfo info) =>
            DokanResult.Success;

        public NtStatus DeleteFile(string fileName, IDokanFileInfo info)
        {
            try
            {
                if (_fs.ReadOnly)
                    throw new ReadOnlyException();
                FileSystemPath fs;
                try
                {
                    fileName = FixName(fileName, false);
                    fs = FileSystemPath.Parse(fileName);
                    if (!_fs.Exists(fs) || info.IsDirectory)
                        return DokanResult.FileNotFound;
                }
                catch (Exception e)
                {
                    _logger.Error("Failed to parse file path\n{0}", e);
                    return DokanResult.Error;
                }
                _fs.Delete(fs);
                return DokanResult.Success;
            }
            catch (ReadOnlyException)
            {
                return DokanResult.AccessDenied;
            }
        }

        public NtStatus DeleteDirectory(string fileName, IDokanFileInfo info)
        {
            try
            {
                if (_fs.ReadOnly)
                    throw new ReadOnlyException();
                FileSystemPath fs;
                try
                {
                    fileName = FixName(fileName, info.IsDirectory);
                    fs = FileSystemPath.Parse(fileName);
                    if (!_fs.Exists(fs) || !info.IsDirectory)
                        return DokanResult.FileNotFound;
                }
                catch (Exception e)
                {
                    _logger.Error("Failed to parse file path\n{0}", e);
                    return DokanResult.Error;
                }
                if (_fs.GetEntities(fs).Any())
                    return DokanResult.DirectoryNotEmpty;
                _fs.Delete(fs);
                return DokanResult.Success;
            }
            catch (ReadOnlyException)
            {
                return DokanResult.AccessDenied;
            }
        }

        public NtStatus MoveFile(string oldName, string newName, bool replace, IDokanFileInfo info)
        {
            if (_fs.ReadOnly)
                return DokanResult.AccessDenied;
            FileSystemPath fs1;
            try
            {
                oldName = FixName(oldName, false);
                fs1 = FileSystemPath.Parse(oldName);
                if (!_fs.Exists(fs1) || info.IsDirectory)
                    return DokanResult.FileNotFound;
            }
            catch (Exception e)
            {
                _logger.Error("Failed to parse file path\n{0}", e);
                return DokanResult.Error;
            }
            FileSystemPath fs2;
            try
            {
                newName = FixName(newName, false);
                fs2 = FileSystemPath.Parse(newName);
                if (_fs.Exists(fs2) && !replace || info.IsDirectory)
                    return DokanResult.AlreadyExists;
            }
            catch (Exception e)
            {
                _logger.Error("Failed to parse file path\n{0}", e);
                return DokanResult.Error;
            }
            try
            {
                _fs.Move(fs1, _fs, fs2);
            }
            catch (ReadOnlyException)
            {
                return DokanResult.AccessDenied;
            }
            catch (Exception e)
            {
                _logger.Error("Failed to move file. Have you registered a file mover?\n{0}", e);
                return DokanResult.Error;
            }
            return DokanResult.Success;
        }

        public NtStatus SetEndOfFile(string fileName, long length, IDokanFileInfo info) => DokanResult.Success;

        public NtStatus SetAllocationSize(string fileName, long length, IDokanFileInfo info) =>
            //TODO find out what on earth this is
            DokanResult.Success;

        public NtStatus LockFile(string fileName, long offset, long length, IDokanFileInfo info)
        {
            StreamContext context = (StreamContext) info.Context;
            if (context.IsLocked)
                return DokanResult.AccessDenied;
            context.IsLocked = true;
            return DokanResult.Success;
        }

        public NtStatus UnlockFile(string fileName, long offset, long length, IDokanFileInfo info)
        {
            StreamContext context = (StreamContext) info.Context;
            if (!context.IsLocked)
                return DokanResult.AccessDenied;
            context.IsLocked = false;
            return DokanResult.Success;
        }

        public NtStatus GetDiskFreeSpace(out long freeBytesAvailable, out long totalNumberOfBytes,
            out long totalNumberOfFreeBytes,
            IDokanFileInfo info)
        {
            _fs.GetSpace(out totalNumberOfBytes, out totalNumberOfFreeBytes);
            totalNumberOfFreeBytes = totalNumberOfBytes - totalNumberOfFreeBytes;
            freeBytesAvailable = totalNumberOfFreeBytes; //8L * 1024 * 1024 * 1024;
            //totalNumberOfFreeBytes = 4L * 1024 * 1024 * 1024;
            //totalNumberOfBytes = freeBytesAvailable + totalNumberOfFreeBytes;
            return DokanResult.Success;
        }

        public NtStatus GetVolumeInformation(out string volumeLabel, out FileSystemFeatures features,
            out string fileSystemName,
            out uint maximumComponentLength, IDokanFileInfo info)
        {
            volumeLabel = _volumeLabel;
            features = FileSystemFeatures.CaseSensitiveSearch | FileSystemFeatures.CasePreservedNames |
                       FileSystemFeatures.UnicodeOnDisk |
                       FileSystemFeatures.PersistentAcls | FileSystemFeatures.SupportsRemoteStorage;
            if (_fs.ReadOnly)
                features |= FileSystemFeatures.ReadOnlyVolume;
            fileSystemName = _fileSystemName;
            maximumComponentLength = 256;
            return DokanResult.Success;
        }

        public NtStatus GetFileSecurity(string fileName, out FileSystemSecurity security,
            AccessControlSections sections,
            IDokanFileInfo info)
        {
            FileSystemPath fs;
            DirectorySecurity ds = new DirectorySecurity();
            FileSecurity fis = new FileSecurity();
            security = info.IsDirectory ? (FileSystemSecurity) ds : fis;
            try
            {
                fileName = FixName(fileName, info.IsDirectory);
                fs = FileSystemPath.Parse(fileName);
                if (!_fs.Exists(fs))
                    return DokanResult.FileNotFound;
            }
            catch (Exception e)
            {
                _logger.Error("Failed to parse file path\n{0}", e);
                return DokanResult.Error;
            }
            security.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null),
                FileSystemRights.FullControl, AccessControlType.Allow));
            return DokanResult.Success;
        }

        public NtStatus SetFileSecurity(string fileName, FileSystemSecurity security, AccessControlSections sections,
            IDokanFileInfo info) =>
            DokanResult.Success;

        public NtStatus Mounted(IDokanFileInfo info) => DokanResult.Success;

        public NtStatus Unmounted(IDokanFileInfo info) => DokanResult.Success;

        public NtStatus FindStreams(string fileName, out IList<FileInformation> streams, IDokanFileInfo info)
        {
            streams = Enumerable.Empty<FileInformation>().ToList();
            return NtStatus.NotImplemented;
        }

        private string FixName(string fileName, bool isDirectory)
        {
            fileName = fileName.Replace("\\", "/");
            if (isDirectory && !fileName.EndsWith("/"))
                fileName = fileName + "/";
            if (!isDirectory && fileName.EndsWith("/"))
                fileName = fileName.Substring(0, fileName.Length - 1);
            return fileName;
        }

        private IList<FileInformation> GetFileInfo(IEnumerable<FileSystemPath> paths)
        {
            return paths.Select(s => new Tuple<NtStatus, FileInformation>(GetFileInformation(s.Path,
                    out FileInformation info, new MockDokanFileInfo
                    {
                        IsDirectory = s.IsDirectory
                    }), info))
                .Where(s => s.Item1 == NtStatus.Success).Select(s => s.Item2).ToList();
        }
    }
}