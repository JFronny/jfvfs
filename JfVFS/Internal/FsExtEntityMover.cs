﻿using System;
using SharpFileSystem;

namespace JfVfs.Internal
{
    internal class FsExtEntityMover : IEntityMover
    {
        private readonly Action<IFileSystem, FileSystemPath, IFileSystem, FileSystemPath> _move;
        public FsExtEntityMover(Action<IFileSystem, FileSystemPath, IFileSystem, FileSystemPath> move) => _move = move;

        public void Move(IFileSystem source, FileSystemPath sourcePath, IFileSystem destination,
            FileSystemPath destinationPath) => _move(source, sourcePath, destination, destinationPath);
    }
}