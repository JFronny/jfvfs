﻿using System;
using System.IO;
using System.Threading.Tasks;
using SharpFileSystem;
using FileAccess = DokanNet.FileAccess;

namespace JfVfs.Internal
{
    internal class StreamContext : IDisposable
    {
        public StreamContext(FileSystemPath file, FileAccess access)
        {
            File = file;
            Access = access;
        }

        public FileSystemPath File { get; }

        public FileAccess Access { get; }

        public Stream Stream { get; set; }

        public Task Task { get; set; }

        public bool IsLocked { get; set; }

        public bool CanWriteDelayed =>
            Access.HasFlag(FileAccess.WriteData) && (Stream?.CanRead ?? false) && Task == null;

        public void Dispose()
        {
            Stream?.Dispose();
        }
    }
}