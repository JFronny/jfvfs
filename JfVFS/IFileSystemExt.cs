﻿using JfVfs.Internal;
using SharpFileSystem;

namespace JfVfs
{
    public interface IFileSystemExt : IFileSystem
    {
        bool ReadOnly { get; }

        void Move(IFileSystem destinationSystem, FileSystemPath source, FileSystemPath destination) =>
            FileSystemW._mover.Move(this, source, destinationSystem, destination);

        void Copy(IFileSystem destinationSystem, FileSystemPath source, FileSystemPath destination) =>
            FileSystemW._copier.Copy(this, source, destinationSystem, destination);

        void GetSpace(out long totalSize, out long usedSpace)
        {
            totalSize = 8L * 1024 * 1024 * 1024;
            usedSpace = 3L * 1024 * 1024 * 1024;
        }
    }
}