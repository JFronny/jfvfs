﻿using System;
using System.IO;
using System.Reflection;

namespace GetVer
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Assembly asm = Assembly.LoadFile(Path.GetFullPath(args[0]));
            Console.WriteLine(asm.GetName().Version);
        }
    }
}